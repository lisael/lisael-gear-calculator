const RINGS = [35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54]
// const RINGS = [37];
const COGS = [22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11];
const PI = Math.PI;
var resultTable: (Result | null)[][] = [];
var currentDetailed: [number, number] | null = null;

function toInches(mm: number): number {
        return mm / 25.4;
}

function toMilimeters(i: number): number {
        return i * 25.4;
}

class Result {
        ring: number;
        cog: number;
        dist: number;
        slack: number;
        chainLength: number;

        constructor(ring: number, cog: number, dist: number, slack: number, chainLength: number) {
                this.ring = ring;
                this.cog = cog;
                this.dist = dist;
                this.slack = slack;
                this.chainLength = chainLength;
        }
}

function gcd(x: number, y: number): number {
        while (y) {
                var t = y;
                y = x % y;
                x = t;
        }
        return x;
}

function skidPatches(ring: number, cog: number, ambidextrous = false): number {
        const _gcd = gcd(cog, ring);
        cog = cog / _gcd;
        if (ambidextrous && (ring / _gcd) % 2 == 1)
                return cog * 2
        else
                return cog
}

class Calculator {
        minDist: number;
        maxDist: number;
        minRatio: number;
        maxRatio: number;
        chainLength: (number | null);
        minSlack: number;
        minSlackOpt: number;
        maxSlack: number;
        maxSlackOpt: number;
        cogs: number[];
        rings: number[];
        imperial: boolean;
        result: (Result | null)[][];

        constructor(
                minDist: number,
                maxDist: number,
                chainLength: (number | null),
                minRatio = 2,
                maxRatio = 4,
                minSlack = 0.30,
                maxSlack = 0.75,
                cogs = COGS,
                rings = RINGS,
                imperial = false,
        ) {
                if (imperial) {
                        this.minDist = minDist;
                        this.maxDist = maxDist;
                } else {
                        this.minDist = toInches(minDist);
                        this.maxDist = toInches(maxDist);
                }
                this.minRatio = minRatio;
                this.maxRatio = maxRatio;
                this.chainLength = chainLength;
                this.minSlack = minSlack;
                const slackDelta = maxSlack - minSlack;
                this.minSlackOpt = minSlack + slackDelta / 4;
                this.maxSlack = maxSlack;
                this.maxSlackOpt = maxSlack - slackDelta / 4;
                this.cogs = cogs;
                this.rings = rings;
                this.imperial = imperial;
                this.result = initResults(rings, cogs);
        }

        run(): void {
                if (this.chainLength !== null) {
                        return this.runWithChainlength();
                } else {
                        return this.runWithoutChainlength();
                }
        }

        runWithoutChainlength(): void {
                for (var i in this.rings) {
                        const ring = this.rings[i];
                        for (var ii in this.cogs) {
                                const cog = this.cogs[ii];
                                const ratio = ring / cog;
                                if (ratio > this.maxRatio) {
                                        break;
                                }
                                if (ratio < this.minRatio) {
                                        continue;
                                }
                                var dist = this.minDist;
                                var [length, slack] = this.chainLengthFor(ring, cog, dist);
                                while ((slack < this.minSlack || slack > this.maxSlack) && dist <= this.maxDist) {
                                        dist += 0.005;
                                        [length, slack] = this.chainLengthFor(ring, cog, dist);
                                }
                                if (dist > this.maxDist) {
                                        continue;
                                }
                                this.result[i][ii] = new Result(ring, cog, dist, slack, length);
                        }
                }
        }

        runWithChainlength(): void {
                if (this.chainLength === null) {
                        return;
                }
                for (var i in this.rings) {
                        const ring = this.rings[i];
                        for (var ii in this.cogs) {
                                const cog = this.cogs[ii];
                                const ratio = ring / cog;
                                if (ratio > this.maxRatio) {
                                        break;
                                }
                                if (ratio < this.minRatio) {
                                        continue;
                                }
                                var [dist, slack] = this.gearDist(ring, cog);
                                if (dist < this.minDist) {
                                        // too short but maybe the calculation resulted in a large slack.
                                        // try with minDist.
                                        dist = this.minDist;
                                        [length, slack] = this.chainLengthFor(ring, cog, dist);
                                        if (length == this.chainLength && slack >= this.minSlack) {
                                        } else {
                                                continue
                                        }
                                } else if (dist > this.maxDist) {
                                        // too long but maybe the calculation resulted in a smal slack.
                                        // try with maxDist.
                                        dist = this.maxDist;
                                        [length, slack] = this.chainLengthFor(ring, cog, dist);
                                        if (length == this.chainLength && slack <= this.maxSlack) {
                                        } else {
                                                break;
                                        }
                                }
                                this.result[i][ii] = new Result(ring, cog, dist, slack, this.chainLength);
                        }
                }
        }

        chainLengthFor(ring, cog, dist): [number, number] {
                const rRing = ring / (4 * PI);
                const rCog = cog / (4 * PI);
                const op = rRing - rCog;
                const straight = Math.sqrt((dist ** 2) - (op ** 2));
                var length = 2 * straight;
                length += ring / 4;
                length += cog / 4;
                const sina = op / dist;
                const a = Math.asin(sina);
                length += (rRing / 2) * a;
                length -= (rCog / 2) * a;
                const slack = this.getSlack(straight, length);
                return [Math.ceil(length), slack];
        }

        getSlack(straight: number, length: number): number {
                const realLen = Math.ceil(length);
                const rest = realLen - length;
                return Math.sqrt(
                        ((straight + rest) / 2) ** 2 - (straight / 2) ** 2);
        }

        gearDist(ring: number, cog: number, minSlack = -1, maxSlack = -1): [number, number] {
                if (this.chainLength === null) {
                        return [-1, -1];
                }
                minSlack = (minSlack === -1) ? this.minSlack : minSlack;
                maxSlack = (maxSlack === -1) ? this.maxSlack : maxSlack;

                // teeth = 1 inch -> radius = (teeth / 2) / 2pi
                const rRing = ring / (4 * PI);
                const rCog = cog / (4 * PI);

                // start with the ring and cog in contact, set them appart, then
                // bisect when the chain length is too long
                var dist = rRing + rCog;
                var [length, slack] = this.chainLengthFor(ring, cog, dist);
                if (length > this.chainLength) {
                        return [-1, -1];
                }
                var bottom = dist;
                var top: number | null = null;
                while (true) {
                        if (length === this.chainLength) {
                                if (slack > maxSlack) {
                                        bottom = dist;
                                        if (top === null) {
                                                dist += 0.1;
                                        } else {
                                                dist = (dist + top) / 2;
                                        }
                                } else if (slack < minSlack) {
                                        top = dist;
                                        dist = (bottom + dist) / 2;
                                } else {
                                        return [dist, slack];
                                }

                        } else if (length < this.chainLength) {
                                bottom = dist;
                                if (top === null) {
                                        dist += 0.5;
                                } else {
                                        dist = (dist + top) / 2;
                                }
                        } else { // length > this.chainLength
                                top = dist;
                                dist = (bottom + dist) / 2;
                        }
                        [length, slack] = this.chainLengthFor(ring, cog, dist);
                }
        }
}

function gentable(rings: number[], cogs: number[]) {
        const app = document.getElementById("app");
        var table = document.getElementById("table");
        if (table === null) {
                table = document.createElement("table");
                table.id = "table";
                table.classList.add("result-table");
                app?.appendChild(table);
        } else {
                table.innerHTML = "";
        }
        const header = document.createElement("tr");
        header.appendChild(document.createElement("th"));
        for (var r of rings) {
                const cell = document.createElement("th");
                cell.textContent = r.toString();
                header.appendChild(cell);
        }
        table.appendChild(header);
        for (var c_i in cogs) {
                const row = document.createElement("tr");
                const cell = document.createElement("th");
                cell.textContent = cogs[c_i].toString();
                row.appendChild(cell);
                for (var r_i in rings) {
                        const cell = document.createElement("td");
                        const result = resultTable[r_i][c_i];
                        if (result !== null) {
                                const link = document.createElement("a");
                                link.href = `javascript:showDetails(${r_i}, ${c_i});`;
                                const ratio = result.ring / result.cog;
                                link.textContent = ratio.toFixed(2).toString();
                                cell.appendChild(link);
                        } else {
                                cell.textContent = "";
                        }
                        row.appendChild(cell);
                }
                table.appendChild(row);
        }
}

function initResults(rings: number[], cogs: number[]): (Result | null)[][] {
        const results: (Result | null)[][] = [];
        for (var i in rings) {
                const row: (Result | null)[] = [];
                for (var ii in cogs) {
                        row.push(null);
                }
                results.push(row);
        }
        return results;
}

function refreshDetails() {
        if (currentDetailed !== null)
                showDetails(currentDetailed[0], currentDetailed[1]);
}

class DataError {
        field: string
        details: string = ""

        constructor(field: string, details: string) {
                this.field = field
                this.details = details
        }
}

const range = (start, end) => Array.from(Array(end - start + 1).keys()).map(x => x + start);

function parseCogs(cogs: string): (null | number[]) {
        try {
                const parts = cogs.split(",").map(
                        function(e) {
                                const part = e.trim();
                                if (part.includes("-")) {
                                        const [start, end] = part.split("-").map(function(ee) { return Number(ee.trim()) });
                                        return range(start, end);
                                } else {
                                        return Number(part);
                                }
                        }
                );
                return parts.flat().filter(function(e) { return !isNaN(e) });
        } catch (e) {
                return null;
        }
}

class Data {
        min_dist: number
        max_dist: number
        chain_length: (number | null)
        min_ratio: number
        max_ratio: number
        min_slack: number
        max_slack: number
        ring_list: number[]
        cog_list: number[]
        errors: DataError[] | null = null

        constructor() {
                this.min_dist = Number((<HTMLInputElement>document.getElementById("min_dist")).value);
                if (this.max_dist <= 0) {
                        this.addError("min_dist", "Must be > 0");
                }
                this.max_dist = Number((<HTMLInputElement>document.getElementById("max_dist")).value);
                if (this.min_dist >= this.max_dist) {
                        this.addError("max_dist", "Must be longer than min");
                }
                const length = (<HTMLInputElement>document.getElementById("chain_length")).value;
                if (length.trim() == "") {
                        this.chain_length = null;
                }
                else {
                        this.chain_length = Number(length);
                        if (this.chain_length <= 0) {
                                this.addError("chain_length", "Must be > 0 or not set");
                        }
                }
                this.min_ratio = Number((<HTMLInputElement>document.getElementById("min_ratio")).value);
                if (this.min_ratio <= 0) {
                        this.addError("min_ratio", "Must be > 0");
                }
                this.max_ratio = Number((<HTMLInputElement>document.getElementById("max_ratio")).value);
                if (this.min_ratio >= this.max_ratio) {
                        this.addError("max_ratio", "Must be larger than min");
                }
                this.min_slack = Number((<HTMLInputElement>document.getElementById("min_slack")).value);
                if (this.min_slack <= 0) {
                        this.addError("min_slack", "Must be > 0");
                }
                this.max_slack = Number((<HTMLInputElement>document.getElementById("max_slack")).value);
                if (this.min_slack >= this.max_slack) {
                        this.addError("max_slack", "Must be larger than min");
                }
                const ring_list = (<HTMLInputElement>document.getElementById("ring_list")).value;
                const rings = parseCogs(ring_list);
                if (rings === null || rings.length === 0) {
                        this.addError("ring_list", "Invalid list specification");
                        this.ring_list = [];
                } else {
                        this.ring_list = rings.sort();
                }
                const cog_list = (<HTMLInputElement>document.getElementById("cog_list")).value;
                const cogs = parseCogs(cog_list);
                if (cogs === null || cogs.length === 0) {
                        this.addError("cog_list", "Invalid list specification");
                        this.cog_list = [];
                } else {
                        this.cog_list = cogs.sort().reverse();
                }
        }

        addError(field: string, details: string) {
                if (this.errors === null) {
                        this.errors = []
                }
                this.errors.push(new DataError(field, details));
        }
}

function showDetails(ringIdx: number, cogIdx: number) {
        const result = resultTable[ringIdx][cogIdx];
        if (result === null) {
                currentDetailed = null;
                (<HTMLTableCellElement>document.getElementById("result-title")).textContent = "Click on a ratio";
                (<HTMLTableCellElement>document.getElementById("result-skid-patches")).textContent = "";
                (<HTMLTableCellElement>document.getElementById("result-chain-length")).textContent = "";
                (<HTMLTableCellElement>document.getElementById("result-dist")).textContent = "";
                (<HTMLTableCellElement>document.getElementById("result-slack")).textContent = "";
                const body = <HTMLTableSectionElement>document.getElementById("details-body");
                body.classList.remove("details-visible");
                body.classList.add("details-hidden");
        } else {
                currentDetailed = [ringIdx, cogIdx];
                const amb = (<HTMLInputElement>document.getElementById("ambidextrous")).checked;
                const patches = skidPatches(result.ring, result.cog, amb);
                (<HTMLTableCellElement>document.getElementById("result-skid-patches")).textContent = patches.toString();
                (<HTMLTableCellElement>document.getElementById("result-title")).textContent = `${result.ring} × ${result.cog}`;
                (<HTMLTableCellElement>document.getElementById("result-chain-length")).textContent = result.chainLength.toString();
                (<HTMLTableCellElement>document.getElementById("result-dist")).textContent = `${toMilimeters(result.dist).toFixed(0).toString()}mm`;
                (<HTMLTableCellElement>document.getElementById("result-slack")).textContent = result.slack.toFixed(2).toString();
                const body = <HTMLTableSectionElement>document.getElementById("details-body");
                body.classList.remove("details-hidden");
                body.classList.add("details-visible");
        }
}

function gearcalc() {
        const errors = Array.from(document.getElementsByClassName("error"));
        for (var e of errors) {
                e.classList.remove("error-visible");
                e.classList.add("error-hidden");
                e.textContent = "";
        }
        const data = new Data();
        if (data.errors !== null) {
                for (var de of data.errors) {
                        const errSpan = document.getElementById(`${de.field}_error`);
                        if (errSpan === null) continue;
                        errSpan.textContent = de.details;
                        errSpan.classList.remove("error-hidden");
                        errSpan.classList.add("error-visible");
                }
                return;
        }
        const calc = new Calculator(
                data.min_dist,
                data.max_dist,
                data.chain_length,
                data.min_ratio,
                data.max_ratio,
                data.min_slack,
                data.max_slack,
                data.cog_list,
                data.ring_list,
        );
        calc.run();
        resultTable = calc.result;
        gentable(calc.rings, calc.cogs);
        refreshDetails();
}
